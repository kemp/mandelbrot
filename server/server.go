package main

import (
	"image/jpeg"
	"net/http"
	"runtime"
	"strconv"
	"strings"

	"bitbucket.org/kemp/mandelbrot"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	http.HandleFunc("/", handleMarkupRequest)
	http.HandleFunc("/image", handleImageRequest)
	http.ListenAndServe(":1234", nil)
}

func handleMarkupRequest(writer http.ResponseWriter, request *http.Request) {
	query := request.URL.RawQuery
	if len(query) == 0 {
		query = "v=3.0,2.0,-2.0,-1.0"
	}
	writer.Write([]byte(`<html>
		<head>
			<title>Fractal Viewer</title>
			<style>
				img {border:1px solid black}
			</style>
			<script>
			<!--

			function zoom(event) {
				image = document.getElementById("image")
				image.style.cursor = 'wait';
				v = image.src.split("?")[1].substr(2).split(',')
				x = event.offsetX / 640 * v[0] + parseFloat(v[2]);
				y = event.offsetY / 480 * v[1] + parseFloat(v[3]);
				v[0] *= 0.5;
				v[1] *= 0.5;
				image.src = "/image?v=" + v[0] + "," + v[1] + "," + (x - v[0]/2) +
						"," + (y - v[1]/2);
			}

			function loaded() {
				image = document.getElementById("image")
				image.style.cursor = 'default';
			}

			//-->
			</script>
		</head>
		<body>
			<img id="image" src="/image?` + query + `"
					onclick="zoom(event)" onload="loaded()"/><br />
			<button onclick="window.location='/'">Reset</button>
		</body>
	</html>`))
}

func handleImageRequest(writer http.ResponseWriter, request *http.Request) {
	values := strings.Split(request.URL.Query().Get("v"), ",")
	v := make([]float64, len(values))
	for i := range values {
		v[i], _ = strconv.ParseFloat(values[i], 64)
	}
	writer.Header().Set("Content-Type", "image/jpeg")
	viewport := &mandelbrot.Viewport{v[0], v[1], v[2], v[3]}
	jpeg.Encode(writer, mandelbrot.NewImage(viewport, 640, 480), nil)
}
