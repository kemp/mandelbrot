package mandelbrot

// Take a point called Z in the complex plane.
// Let Z1 be Z squared plus Z
// Z2 is Z1 squared plus Z
// Z3 is Z2 squared plus Z
//
// If the series of Zs should always stay
// close to Z and never trend away
// that point is in the Mandelbrot set.

import (
	"image"
	"image/color"
	"runtime"
)

const (
	maximumIterations = 1000
)

var (
	palette = make([]color.Color, 256)
)

func init() {
	for i := range palette {
		palette[i] = &color.Gray{uint8(255 - i)}
	}
	runtime.GOMAXPROCS(runtime.NumCPU())
}

type Viewport struct {
	Width, Height, Left, Top float64
}

type mandelbrot struct {
	width  int
	colors []uint8
	ready  chan struct{}
}

func NewImage(viewport *Viewport, width, height int) image.Image {
	p := make([]float64, width)
	for i := 0; i < width; i++ {
		p[i] = float64(i)/float64(width)*viewport.Width + viewport.Left
	}

	result := &mandelbrot{
		width, make([]uint8, width*height), make(chan struct{}),
	}

	completed := make(chan bool, height)
	for i := 0; i < height; i++ {
		go func(i int, q float64) {
			for j := 0; j < width; j++ {
				result.colors[i*width+j] = compute(p[j], q)
			}
			completed <- true
		}(i, float64(i)/float64(height)*viewport.Height+viewport.Top)
	}

	go func() {
		for count := 0; count < height; count++ {
			<-completed
		}
		close(result.ready)
	}()

	return result
}

func compute(x, y float64) uint8 {
	c := complex(x, y)
	for k, z := 0, c; k < maximumIterations; k++ {
		dx, dy := real(z)-real(c), imag(z)-imag(c)
		if dx*dx+dy*dy > 4 {
			return uint8(k % 256)
		}
		z = z*z + c
	}
	return 255
}

func (this *mandelbrot) ColorModel() color.Model { return color.RGBAModel }

func (this *mandelbrot) Bounds() image.Rectangle {
	<-this.ready
	return image.Rect(0, 0, this.width, len(this.colors)/this.width)
}

func (this *mandelbrot) At(x, y int) color.Color {
	return palette[this.colors[y*this.width+x]]
}
