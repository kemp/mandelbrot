package main

import (
	"flag"
	"image/jpeg"
	"log"
	"os"

	"bitbucket.org/kemp/mandelbrot"
)

var (
	filename = flag.String("o", "output.jpg", "Output filename")
)

func main() {
	flag.Parse()

	result := mandelbrot.NewImage(&mandelbrot.Viewport{3.0, 2, -2.0, -1}, 640, 480)
	file, err := os.Create(*filename)
	if err != nil {
		log.Fatal(err)
	}

	if err := jpeg.Encode(file, result, nil); err != nil {
		log.Fatal(err)
	}
}
